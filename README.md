# My Wordpress Theme

A Twenty-Fourteen child theme for Wordpress. 

- Install the parent theme Twenty Fourteen via your Wordpress panel.
- If you wish so, add a favicon.ico in the child theme root directory.
- Transfer the theme into the directory /wp-content/themes/ of your Wordpress site.
- Activate and customize it via your Wordpress panel.