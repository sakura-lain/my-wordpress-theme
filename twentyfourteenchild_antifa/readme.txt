Dépôt : https://gitlab.com/sakura-lain/my-wordpress-theme/https://gitlab.com/sakura-lain/my-wordpress-theme/
Thème dérivé du Twenty Fourteen Child de Wordpress

Transférer fichiers du thème enfant dans le dossier wp-content/themes/nom-du-thème-enfant
Pour les traductions : traduire le thème parent et Wordpress
Pour avoir le titre en rouge (et blanc au survol), voir couleur du titre du site dans la personnalisation du thème (interface interne)
Pour introduire une favicon, copier favicon.ico à la racine du site et à la racine du child theme, joindre le fichier header.php dans le child theme
Transférer content.php pour n'afficher que les extraits en une

Principales couleurs du thème (tons de rouge, du plus clair au plus foncé) :
#c0392b
#dd3333
#d9273c

Pour les vidéos : D2000C

html5 : object param taille lecteur

Mise à jour
- le fichier placement "header" pour la favicon dans le thème enfant ne semble plus utile.
- modification du fichier style.css pour afficher les titres des post-formats en minuscule, les afficher partout et les afficher en capitale dans les widgets.
- modification du fichier style.css pour afficher les infos meta en minuscule (date, auteur)
- modification du fichier style.css pour pour centrer le site
- ajout d'un retour en haut dans le fichier functions.php et d'une fonction permettent de ne pas afficher les post-formats en colonne centrale.
- désactivation de la mise à jour auto des traductions /!\ se fait dans le functions.php du thème parent, ajouter cette ligne :
//Disable translations auto-updates
add_filter( 'auto_update_translation', '__return_false' );
- ajout à style.css d'une fonction pour que les iamges "une" soient agrandies à 100%
- ajout d'un code interdisant les commentaires dans le functions.php
- blocage des emojis : ajout de deux lignes à functions.php :     remove_action('wp_head', 'print_emoji_detection_script', 7);

    remove_action('wp_print_styles', 'print_emoji_styles');

Liens utiles pour sécuriser Wordpress via functions.php :
- https://perishablepress.com/stop-user-enumeration-wordpress/
- https://wp-mix.com/wordpress-disable-author-archives/
- https://kinsta.com/fr/base-de-connaissances/desactiver-flux-rss-wordpress/#2-dsactiver-les-flux-rss-avec-du-code
- (pas très probant) https://medium.com/@pixelvars/wordpress-disable-default-author-pages-752b1b3461ad
