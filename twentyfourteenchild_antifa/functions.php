<?php

add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);
function enqueue_child_theme_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), array('parent-style')  );
}

/**
 * Setup My Child Theme's textdomain.
 *
 * Declare textdomain for this child theme.
 * Translations can be filed in the /languages/ directory.
 */
function my_child_theme_setup() {
	load_child_theme_textdomain( 'my-child-theme', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'my_child_theme_setup' );


/**
 * Excluding post formats from blog
 */

function be_exclude_post_formats_from_blog( $query ) {

	if( $query->is_main_query() && $query->is_home() ) {
		$tax_query = array( array(
			'taxonomy' => 'post_format',
			'field' => 'slug',
			'terms' => array( 'post-format-aside', 'post-format-link', 'post-format-quote', 'post-format-audio', 'post-format-video', 'post-format-image', 'post-format-gallery' ),
			'operator' => 'NOT IN',
		) );
		$query->set( 'tax_query', $tax_query );
	}

}
/**
 * Exlure les post-formats de la page principale
 */
add_action( 'pre_get_posts', 'be_exclude_post_formats_from_blog' );

/**Ajouter une fonction "Retour en haut"
 *
 */
    add_action( 'wp_footer', 'back_to_top' );
    function back_to_top() {
    echo '<a id="totop" href="#">Haut de page &uarr;</a>';
    }

	    add_action( 'wp_head', 'back_to_top_style' );
    function back_to_top_style() {
    echo '<style type="text/css">
    #totop {
    position: fixed;
    right: 30px;
    bottom: 30px;
    display: none;
    outline: none;
    }
    </style>';
    }

	    add_action( 'wp_footer', 'back_to_top_script' );
    function back_to_top_script() {
    echo '<script type="text/javascript">
    jQuery(document).ready(function($){
    $(window).scroll(function () {
    if ( $(this).scrollTop() > 400 )
    $("#totop").fadeIn();
    else
    $("#totop").fadeOut();
    });

    $("#totop").click(function () {
    $("body,html").animate({ scrollTop: 0 }, 800 );
    return false;
    });
    });
    </script>';
    }

add_filter ( 'comments_open' ,  'wpc_comments_closed' ,  10 ,  2 ) ;

function  wpc_comments_closed (  $open ,  $post_id  )  {
$post  =  get_post (  $post_id  ) ;
$open  =  false ;
return  $open ;
}

    remove_action('wp_head', 'print_emoji_detection_script', 7);

    remove_action('wp_print_styles', 'print_emoji_styles');


// block WP enum scans
// https://m0n.co/enum
if (!is_admin()) {
    // default URL format
    if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die();
    add_filter('redirect_canonical', 'shapeSpace_check_enum', 10, 2);
}
function shapeSpace_check_enum($redirect, $request) {
    // permalink URL format
    if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die();
    else return $redirect;
}

// disable author archives
function shapeSpace_disable_author_archives() {

	if (is_author()) {

		global $wp_query;
		$wp_query->set_404();
		status_header(404);

	} else {

		redirect_canonical();

	}

}
remove_filter('template_redirect', 'redirect_canonical');
add_action('template_redirect', 'shapeSpace_disable_author_archives');

// Don't display WP version
remove_action("wp_head", "wp_generator");

// Disable RSS
/*
function itsme_disable_feed() {
 wp_die( __( 'No feed available, please visit the homepage!' ) );
}

add_action('do_feed', 'itsme_disable_feed', 1);
add_action('do_feed_rdf', 'itsme_disable_feed', 1);
add_action('do_feed_rss', 'itsme_disable_feed', 1);
add_action('do_feed_rss2', 'itsme_disable_feed', 1);
add_action('do_feed_atom', 'itsme_disable_feed', 1);
add_action('do_feed_rss2_comments', 'itsme_disable_feed', 1);
add_action('do_feed_atom_comments', 'itsme_disable_feed', 1);
*/

//Disable XML RPC
add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );
